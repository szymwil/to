<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\ApiService;

class ApiController extends Controller
{

    private $badRequestResponseContent;
    private $successResponseContent;

    public function __construct() {
        $this->badRequestResponseContent = ["Bad Request"];
        $this->successResponseContent = ["OK"];
    }

    /**
     * Display a listing of the resource.
     *
     * @param string $model
     * @return \Illuminate\Http\Response
     */
    public function index(string $model)
    {
        $service = new ApiService();
        $instance = $service->getNewClassInstance($model);
        if ($instance === false) return response($service->encodeResponse($this->badRequestResponseContent),400);

        $relations = $instance->relations;

        $result = $instance->with($relations)->get()->toArray();
        return $service->encodeResponse($result);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param string $model
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, string $model)
    {
        $service = new ApiService();
        $instance = $service->getNewClassInstance($model);
        if ($instance === false) return response($service->encodeResponse($this->badRequestResponseContent),400);

        $relations = $instance->relations;
        $attributes = $service->getTableAttributes($instance);

        $validation = $service->validateRequest($request, $instance->getTable(), $attributes, $relations);
        if ($validation === false) return response($service->encodeResponse($this->badRequestResponseContent),400);

        foreach ($attributes as $attribute) {
            $instance->$attribute = $request->$attribute;
        }
        $instance->save();
        if ($relations) {
            foreach ($relations as $relation) {
                $instance->$relation()->sync(json_decode($request->$relation, true));
            }
        }

        return response($service->encodeResponse($this->successResponseContent),200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        //
    }
}
