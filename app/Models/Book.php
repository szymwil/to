<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use HasFactory;

    public $relations = ["authors"];

    public function authors()
    {
        return $this->belongsToMany(Author::class);
    }


    public function genre()
    {
        return $this->belongsTo(Genre::class);
    }

    public function isbn()
    {
        return $this->hasOne(Isbn::class);
    }
}
