<?php

namespace App\Services;
use App\Models\Book;
use App\Models\Author;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class ApiService {

    private $skipModelColumns;

    public function __construct() {
        $this->skipModelColumns = ['id','created_at','updated_at'];
    }

    public function getNewClassInstance(string $model) : object {
        $model = ucfirst($model);
        $class = "App\Models\\$model";
        if (!class_exists($class)) {
            return false;
        }
        return new $class;
    }

    public function getTableAttributes(object $instance) : array {

        return array_diff(Schema::getColumnListing($instance->getTable()), $this->skipModelColumns);

    }

    public function getTableAttributeType(string $table, string $attribute) : string {

        return Schema::getColumnType($table, $attribute);

    }

    private function getValidateRequestRules(string $table, array $attributes) : array {
        $rules = array();
        foreach ($attributes as $attribute) {
            $type = $this->getTableAttributeType($table, $attribute);
            $rules[$attribute] = 'required|'.$type;
        }
        return $rules;
    }

    public function validateRequest(Request $request, string $table, array $attributes, array $relations = null) : bool {
        $validateRules = $this->getValidateRequestRules($table, $attributes);
        if ($relations) {
            foreach ($relations as $relation) {
                $validateRules[$relation] = "required|json";
            }
        }
        $validator = Validator::make($request->all(), $validateRules);
        if ($validator->fails()) {
            return false;
        }
        return true;
    }

    public function encodeResponse(array $result) {
        return json_encode($result);
    }

}
