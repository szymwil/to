<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for($i = 1; $i <=1000; $i++) {
            DB::table('books')->insert([
                'title' => $faker->sentence(4),
                'publish_year' => $faker->year(),
                'publisher' => $faker->company(),
            ]);
        }
    }
}
