<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AuthorsBooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 1; $i <=1000; $i++) {
            for ($j=0; $j<rand(1,3); $j++) {
                DB::table('author_book')->insert([
                    'author_id' => rand(1, 400),
                    'book_id' => $i,
                ]);
            }
        }
    }
}
