<?php

namespace Database\Seeders;

use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AuthorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for($i = 1; $i <=400; $i++) {
            DB::table('authors')->insert([
                'firstname' => $faker->firstName(),
                'lastname' => $faker->lastName(),
            ]);
        }
    }
}
